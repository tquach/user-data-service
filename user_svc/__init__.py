import bottle
import yaml
import os
import sys

from bottle.ext import sqlalchemy

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declared_attr, declarative_base

DEFAULT_CONF = os.path.join(os.path.dirname(__file__), 'conf', 'dev.conf')

config = {}
try:
    filename = os.environ.get('USER_SVC_CONFIG')
    if not filename:
        filename = DEFAULT_CONF

    config = yaml.load(file(filename, 'r'))
    if 'database' not in config:
        raise yaml.YAMLError('Missing database configuration.')
except yaml.YAMLError, exc:
    print "Error in configuration file:", exc
    sys.exit(2)


app = bottle.Bottle()
app.config = config

engine = create_engine(app.config['database']['url'], convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

db_plugin = sqlalchemy.Plugin(
    engine,  # SQLAlchemy engine created with create_engine function.
    Base.metadata,  # SQLAlchemy metadata, required only if create=True.
    keyword='db',  # Keyword used to inject session database in a route (default 'db').
    # If it is true, execute `metadata.create_all(engine)` when plugin is
    # applied (default False).
    create=True,
    commit=True,  # If it is true, plugin commit changes after route is executed (default True).
    # If it is true and keyword is not defined, plugin uses **kwargs argument
    # to inject session database (default False).
    use_kwargs=False,
    create_session=db_session
)

from user_svc.apps.customer.views import customer_routes
customer_routes.install(db_plugin)
app.mount('/customer/', customer_routes)

from user_svc.apps.account.views import account_routes
account_routes.install(db_plugin)
app.mount('/account/', account_routes)

