import bottle
from bottle import HTTPError, request, HTTPResponse
from sqlalchemy.exc import IntegrityError

from user_svc.apps.account import models


account_routes = bottle.Bottle()


@account_routes.get('/:id')
def get(id, db):
    user_account = db.query(models.UserAccount).filter_by(id=id).first()
    if not user_account:
        return HTTPError(404, 'not found.')
    return user_account.as_json()


@account_routes.get('/social/:id')
def get(id, db):
    social_acccount = db.query(models.SocialAccount).filter_by(id=id).first()
    if not social_acccount:
        return bottle.HTTPError(404, 'not found.')
    return social_acccount.as_json()


# Register a social account,  given a provider type
# Request Parms:
#   provider_type - one of facebook, twitter, google
#   A JSON object representing either a social account or crowd surge user account
@account_routes.post('/register/:provider_type')
def register(provider_type, db):
    provider_type = provider_type.upper()
    if provider_type not in models.SocialAccount.social_providers:
        return HTTPError(400, 'invalid provider')

    params = request.json
    if 'provider_type' not in params:
        params['provider_type'] = provider_type

    # Busines logic pending
    social_account = models.SocialAccount(**params)
    try:
        db.add(social_account)
    except IntegrityError as e:
        return HTTPError(400, e)

    return HTTPResponse(status=204)








