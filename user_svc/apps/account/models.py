import uuid
import datetime

from sqlalchemy import Column, BigInteger, String, ForeignKey, DateTime, Boolean, Enum

from user_svc import Base
from user_svc.apps.core import format_date
from user_svc.apps.core.models import GUID, JSONSerializable

from savalidation import ValidationMixin, watch_session
import savalidation.validators as val

status_codes = ['ACTIVE', 'DISABLED', 'INACTIVE']

class UserAccount(Base, ValidationMixin, JSONSerializable):
    __tablename__ = "user_account"
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    uuid = Column(GUID(), default=uuid.uuid4, nullable=False)
    customer_uuid = Column(GUID, ForeignKey('customer.uuid'))
    username = Column(String(255))
    password = Column(String(255))
    last_login = Column(DateTime, default=datetime.datetime.utcnow, nullable=True)
    email_opt_out = Column(Boolean)
    status = Column(Enum(*status_codes))
    date_created = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    last_updated = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)

    def as_json(self):
        return {
            "id": self.id,
            "uuid": str(self.uuid),
            "customer_uuid": str(self.customer_uuid),
            "username": self.username,
            "last_login": format_date(self.last_login),
            "email_opt_out": self.email_opt_out,
            "status": self.status,
            "date_created": format_date(self.date_created),
            "last_updated": format_date(self.last_updated)
        }

    def __repr__(self):
        return "UserAccount[id=%s]" % self.id


class SocialAccount(Base, ValidationMixin, JSONSerializable):
    __tablename__ = "social_account"
    social_providers = ['FACEBOOK', 'TWITTER', 'GOOGLE']
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    uuid = Column(GUID(), default=uuid.uuid4, nullable=False)
    status = Column(Enum(*status_codes))
    customer_uuid = Column(GUID, ForeignKey('customer.uuid'))
    provider_type = Column(Enum(*social_providers), nullable=False)
    provider_user_id = Column(String(255), nullable=False)
    display_name = Column(String(255), nullable=True)
    profile_url = Column(String(255), nullable=True)
    image_url = Column(String(255), nullable=True)
    access_token = Column(String(255), nullable=False)
    secret = Column(String(255))
    refresh_token = Column(String(255))
    expire_time = Column(BigInteger)
    date_created = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    last_updated = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)

    val.validates_constraints()

    def as_json(self):
        return {
            "id": self.id,
            "uuid": str(self.uuid),
            "customer_uuid": str(self.customer_uuid),
            "provider_type": self.provider_type,
            "provider_user_id": self.provider_user_id,
            "status": self.status,
            "display_name": self.display_name,
            "profile_url": self.profile_url,
            "image_url": self.image_url,
            "access_token": self.access_token,
            "secret": self.secret,
            "refresh_token": self.refresh_token,
            "expire_time": self.expire_time,
            "date_created": format_date(self.date_created),
            "last_updated": format_date(self.last_updated)
        }


def __repr__(self):
    return "SocialAccount[id=%s]" % self.id

