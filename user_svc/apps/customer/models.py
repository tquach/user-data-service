import uuid
import datetime
import re
import simplejson as json

from sqlalchemy import Column, BigInteger, String, ForeignKey, DateTime, Enum
from sqlalchemy.orm import relationship, validates
from user_svc.apps.core import format_date

from user_svc.apps.core.models import GUID, JSONSerializable

from user_svc import Base

from savalidation import ValidationMixin
import savalidation.validators as val

class Address(Base, JSONSerializable):
    __tablename__ = "address"
    status_codes = ['ACTIVE', 'DISABLED', 'INACTIVE']
    address_types = ['SHIPPING', 'BILLING', 'HOME']
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    customer_uuid = Column(GUID, ForeignKey('customer.uuid'))
    uuid = Column(GUID, default=uuid.uuid4, nullable=False)
    status = Column(Enum(*status_codes))
    address_type = Column(Enum(*address_types))
    address_line_1 = Column(String(255), nullable=False)
    address_line_2 = Column(String(255))
    municipality = Column(String(255))
    city = Column(String(255))
    state_province = Column(String(255))
    country_code = Column(String(255))
    postal_zipcode = Column(String(255))
    date_created = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    last_updated = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)

    def as_json(self):
        return {
            u'id': self.id,
            u'uuid': str(self.uuid),
            u'address_type': self.address_type,
            u'address_line_1': self.address_line_1,
            u'address_line_2': self.address_line_2,
            u'municipality': self.municipality,
            u'city': self.city,
            u'state_province': self.state_province,
            u'postal_zipcode': self.postal_zipcode,
            u'country_code': self.country_code,
            u'date_created': format_date(self.date_created),
            u'last_updated': format_date(self.last_updated)
        }



class Customer(Base, ValidationMixin, JSONSerializable):
    __tablename__ = 'customer'
    customer_types = ['PERSON', 'COMPANY']
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    customer_type = Column(Enum(*customer_types, name='customer_type'))
    uuid = Column(GUID(), default=uuid.uuid4, nullable=False)
    addresses = relationship("Address", cascade="all, delete-orphan")
    date_created = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    last_updated = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    __mapper_args__ = {
        'polymorphic_on': customer_type,
    }

    def __repr__(self):
        return "Customer[%s]" % self.id


class Person(Customer):
    __mapper_args__ = {'polymorphic_identity': 'PERSON'}
    first_name = Column(String(255), nullable=False)
    last_name = Column(String(255), nullable=False)
    email = Column(String(255), nullable=False, unique=True)

    val.validates_constraints()

    def as_json(self):
        return {
            u'id': self.id,
            u'uuid': str(self.uuid),
            u'customer_type': self.customer_type,
            u'first_name': self.first_name,
            u'last_name': self.last_name,
            u'email': self.email,
            u'addresses': [a.as_json() for a in self.addresses],
            u'date_created': format_date(self.date_created),
            u'last_updated': format_date(self.last_updated)
        }

    def __repr__(self):
        return "Person[id=%s, first_name=%s, last_name=%s, email=%s]" % (self.id, self.first_name, self.last_name, self.email)


class Company(Customer):
    __mapper_args__ = {'polymorphic_identity': 'COMPANY'}
    name = Column(String(255))

    def as_json(self):
        return {
            u'id': self.id,
            u'uuid': str(self.uuid),
            u'customer_type': self.customer_type,
            u'name': self.name,
            u'addresses': [a.as_json() for a in self.addresses],
            u'date_created': format_date(self.date_created),
            u'last_updated': format_date(self.last_updated)
        }

    # val.validates_constraints()

    def __repr__(self):
        return "Organization[id=%s, name=%s]" % (self.id, self.name)
