import bottle
from sqlalchemy.exc import IntegrityError
from bottle import HTTPResponse, HTTPError, request

from user_svc.apps.customer import models

customer_routes = bottle.Bottle()


@customer_routes.get('/')
def list(db):
    customers = db.query(models.Customer).all()
    return {"customers": [c.as_json() for c in customers]}


@customer_routes.get('/:id')
def get(id, db):
    customer = db.query(models.Customer).filter_by(id=id).first()
    if customer:
        return customer.as_json()

    return HTTPError(404, 'not found.')


@customer_routes.post('/')
def save(db):
    params = request.json

    if params and 'customer_type' in params:
        customer_type = params['customer_type']
    else:
        return HTTPError(400, 'Bad request.')

    model = None
    if customer_type == 'ORGANIZATION':
        model = models.Organization(**params)
    else:

        model = models.Person(**params)

    if 'id' in params:
        customer_id = params['id']
        existing = db.query(models.Customer).filter_by(id=id).first()
        if not existing:
            return HTTPError(404)

        db.merge(model)
    else:
        # Validate model
        db.add(model)

    return HTTPResponse(status=204)


@customer_routes.post('/:id/address')
def add_address(id, db):
    params = request.json
    customer = db.query(models.Customer).filter_by(id=id).first()
    if not customer:
        return HTTPResponse(status=404)

    address = models.Address(**params)
    address.customer_id = customer.id

    try:
        db.add(address)
    except IntegrityError as e:
        return HTTPError(400, e)

    return HTTPResponse(status=204)
