
# Format a date object
def format_date(val):
    return val if not val else val.isoformat()
