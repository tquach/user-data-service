-- Drop it likes it hot
drop table if exists social_account;
drop table if exists user_account;

drop table if exists address;
drop table if exists customer;

drop type if exists status_code;
drop type if exists customer_type;
drop type if exists address_type;
drop type if exists social_provider_type;


-- Types as enums
create type status_code as ENUM ('ACTIVE', 'DISABLED', 'INACTIVE', 'PENDING');
create type customer_type as ENUM ('PERSON', 'COMPANY');
create type address_type as ENUM ('SHIPPING', 'BILLING', 'HOME');
create type social_provider_type as ENUM ('FACEBOOK', 'TWITTER', 'GOOGLE');

-- create type account_type as ENUM ('GUEST', 'USER')

-- Single-table inheritance with a discriminator column customer_type
create table customer (
  id serial primary key,
  uuid uuid not null unique,
  customer_type customer_type not null,
  name varchar(255),
  description varchar(255),
  first_name varchar(255),
  last_name varchar(255),
  email varchar(255) unique,
  date_created timestamp not null default current_timestamp,
  last_updated timestamp not null default current_timestamp
);

alter sequence customer_id_seq RESTART WITH 1000;

create table address (
  id serial primary key,
  uuid uuid not null unique,
  customer_uuid uuid not null references customer(uuid),
  address_type address_type not null,
  address_line_1 varchar(255),
  address_line_2 varchar(255),
  municipality varchar(255),
  city varchar(255),
  state_province varchar(255),
  country_code varchar(10),
  postal_zipcode varchar(25),
  status status_code not null,
  date_created timestamp not null default current_timestamp,
  last_updated timestamp not null default current_timestamp
);

create table user_account (
  id serial primary key,
  uuid uuid not null unique,
  customer_uuid uuid not null references customer(uuid),
  username varchar(255) not null unique,
  password varchar(255) not null,
  last_login timestamp,
  email_opt_out boolean not null default true,
  status status_code not null,
  date_created timestamp not null default current_timestamp,
  last_updated timestamp not null default current_timestamp
);

alter sequence user_account_id_seq RESTART WITH 1000;

create table social_account (
  id serial primary key,
  uuid uuid not null unique,
  customer_uuid uuid not null references customer(uuid),
  status status_code not null,
  provider_type social_provider_type not null,
  provider_user_id varchar(255),
  display_name varchar(255),
  profile_url varchar(512),
  image_url varchar(512),
  access_token varchar(255) not null,          
  secret varchar(255),
  refresh_token varchar(255),
  expire_time bigint,
  unique (id, provider_type, provider_user_id),
  date_created timestamp not null default current_timestamp,
  last_updated timestamp not null default current_timestamp
);