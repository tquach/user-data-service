import multiprocessing

bind = "127.0.0.1:8000"
workers = multiprocessing.cpu_count() * 2 + 1
pid = "PID"
accesslog = "logs/accesslog"
errorlog = "logs/errorlog"
loglevel = "info"
