
import com.excilys.ebi.gatling.core.Predef._
import com.excilys.ebi.gatling.http.Predef._
import com.excilys.ebi.gatling.jdbc.Predef._
import com.excilys.ebi.gatling.http.Headers.Names._

import akka.util.duration._

import bootstrap._
import assertions._
import scala.util.parsing.json._

class CreateCustomerScenario extends Simulation {

  val customFeeder = new Feeder[String] {
    import faker._
    import scala.util.Random

    private val RNG = new Random

    private def randInt(a: Int, b: Int) = RNG.nextInt(b - a) + a

    // always return true as this feeder can be polled infinitively
    override def hasNext = true

    override def next: Map[String, String] = {
      val email = scala.math.abs(java.util.UUID.randomUUID.getMostSignificantBits) + "_gatling@dontsend.com"

      Map(
        "first_name" -> Name.first_name,
        "last_name" -> Name.last_name,
        "email" -> email,
        "companyName" -> Company.name,
        "customerId" -> randInt(1000, 1002).toString)
    }
  }

  val httpConf = httpConfig
    // .baseURL("http://l-web-5.crdsrg.net")
    .baseURL("http://localhost:8000")
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip,deflate,sdch")
    .acceptLanguageHeader("en-US,en;q=0.8")

  val headers_1 = Map(
    "Cache-Control" -> """no-cache""",
    "Content-Type" -> """application/json""")

  val requestBody = JSONObject(Map(
    "first_name" -> "${first_name}",
    "last_name" -> "${last_name}",
    "email" -> "${email}",
    "customer_type" -> "PERSON")).toString()

  val scn = scenario("Create Customer")
    .feed(customFeeder)
    .exec(
      http("Post person")
        .post("/customer/")
        .headers(headers_1)
        .body(requestBody)
        .check(status.is(204)))

  // val orgScn = scenario("Create Organization")
  //   .feed(customFeeder)
  //   .exec(
  //     http("Post company")
  //       .post("/customer/")
  //       .headers(headers_1)
  //       .body(JSONObject(Map(
  //         "name" -> "${companyName}",
  //         "customer_type" -> "ORGANIZATION")).toString())
  //       .check(status.is(204)))

  val addrScn = scenario("Add address, retrieve customer with new address")
    .feed(customFeeder)
    .exec(
      http("Add address")
        .post("/customer/" + "${customerId}" + "/address")
        .headers(headers_1)
        .body(JSONObject(Map(
          "state_province" -> "RJ",
          "city" -> "RIO DE JANEIRO",
          "municipality" -> "RIO DE JANEIRO",
          "country_code" -> "BR",
          "address_type" -> "BILLING",
          "postal_zipcode" -> "11525 - 232",
          "address_line_1" -> "RUA DOS BOQUTES, 1332 ",
          "status" -> "ACTIVE")).toString())
        .check(status.is(204)))

  val customerRetrievalScn = scenario("Retrieve random customer")
    .feed(customFeeder)
    .exec(
      http("Get customer")
        .get("/customer/" + "${customerId}")
        .headers(headers_1)
        .check(status.is(200)))

  setUp(scn.users(1000).ramp(90).protocolConfig(httpConf))
  setUp(addrScn.users(500).ramp(90).delay(2).protocolConfig(httpConf))
  setUp(customerRetrievalScn.users(1000).ramp(90).delay(2).protocolConfig(httpConf))
}