#!/bin/sh

BIN_DIR=`dirname $0`
cd ${BIN_DIR}/.. && APP_HOME=`pwd`

USER_SVC_CONFIG=${USER_SCV_CONFIG:=$APP_HOME/app.conf}

WORKERS=4

export APP_HOME USER_SVC_CONFIG

gunicorn user_svc:app -w ${WORKERS} -D --error-logfile logs/error.log --access-logfile logs/access.log
