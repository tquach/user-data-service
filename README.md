## Overview
Basic proof of concept demonstrating a large application built with Bottle.

_Application Stack_

* Bottle.py
* SQLAlchemy
* Postgresql

### Development

1. Install virtualenv, virtualenvwrapper, etc.
2. `pip install -r requirements.txt`
3. `python manage.py` or `gunicorn user_svc:app`

## Running the server
There are two ways to run the server, using the manage.py script or using `gunicorn`. The configuration for gunicorn is in `gunicorn.conf.py`. You can use uWSGI to deploy alongside nginx.